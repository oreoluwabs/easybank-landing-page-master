import { ChakraProvider, ColorModeScript, extendTheme } from "@chakra-ui/react";

const myTheme = extendTheme({
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
  styles: {
    global: {
      html: {
        fontSize: "115%",
      },
      body: {
        color: "primary.darkBlue",
      },
      a: {
        // color: "teal.500",
        _hover: {
          textDecoration: "none",
        },
        _visited: {
          textDecoration: "none",
        },
        _active: {
          textDecoration: "none",
        },
      },
    },
  },
  fonts: {
    body: "Public Sans, sans-serif",
  },
  fontWeights: {
    light: 300,
    normal: 400,
    bold: 700,
  },

  colors: {
    primary: {
      darkBlue: "hsl(233, 26%, 24%)",
      limeGreen: "hsl(136, 65%, 51%)",
      brightCyan: "hsl(192, 70%, 51%)",
    },
    neutral: {
      grayishBlue: "hsl(233, 8%, 62%)",
      limeGreen: "hsl(220, 16%, 96%)",
      veryLightGray: "hsl(0, 0%, 98%)",
      white: "hsl(0, 0%, 100%)",
    },
  },
  breakpoints: {
    "2xl": "90em",
    "3xl": "122.5em",
  },
});

const MyThemeProvider = ({ children }) => {
  return (
    <>
      <ColorModeScript initialColorMode={myTheme.config.initialColorMode} />
      <ChakraProvider theme={myTheme}>{children}</ChakraProvider>
    </>
  );
};

export default MyThemeProvider;
