import MyThemeProvider from "./theme";

const RootProviders = ({ children }) => {
  return (
    <>
      <MyThemeProvider>{children}</MyThemeProvider>
    </>
  );
};

export default RootProviders;
