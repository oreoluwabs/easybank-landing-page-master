import { Box } from "@chakra-ui/react";
import {
  BenefitsSection,
  HeroSection,
  LatestArticlesSection,
} from "../../components/Home";

const HomePage = () => {
  return (
    <>
      <Box overflowX={{ lg: "hidden" }}>
        <HeroSection />
        <BenefitsSection />
        <LatestArticlesSection />
      </Box>
    </>
  );
};

export default HomePage;
