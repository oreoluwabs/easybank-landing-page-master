import { Box, Flex, HStack, Image, Link, Text } from "@chakra-ui/react";
import { Link as RRLink } from "react-router-dom";
import Logo from "../../assets/images/logo.svg";
import TwitterIcon from "../../assets/images/icon-twitter.svg";
import FacebookIcon from "../../assets/images/icon-facebook.svg";
import YoutubeIcon from "../../assets/images/icon-youtube.svg";
import PinterestIcon from "../../assets/images/icon-pinterest.svg";
import InstagramIcon from "../../assets/images/icon-instagram.svg";
import { RoundedButton } from "../Button";

const Footer = () => {
  return (
    <Box as="footer" backgroundColor="primary.darkBlue" color="neutral.white">
      <Flex
        maxWidth="85%"
        mx="auto"
        alignItems={{ base: "center" }}
        justifyContent={{ base: "center" }}
        flexDirection={{ base: "column", lg: "row" }}
        py={{ base: "2rem" }}
      >
        <Flex
          flexDirection={{ base: "column" }}
          alignSelf={{ lg: "stretch" }}
          justifyContent={{ lg: "space-between" }}
          flex={{ lg: 2 }}
        >
          <Link as={RRLink} to="/">
            <Image src={Logo} alt="Logo" color="white" fill="white" />
          </Link>

          <Box my={{ base: "1.5rem", lg: "1rem" }}>
            <HStack spacing={4}>
              <Link href="/" isExternal _hover={{ color: "primary.limeGreen" }}>
                <Image
                  src={FacebookIcon}
                  alt="facebook account link"
                  color="white"
                  fill="white"
                />
              </Link>
              <Link href="/" isExternal _hover={{ color: "primary.limeGreen" }}>
                <Image
                  src={YoutubeIcon}
                  alt="youtube account link"
                  color="white"
                  fill="white"
                />
              </Link>
              <Link href="/" isExternal _hover={{ color: "primary.limeGreen" }}>
                <Image
                  src={TwitterIcon}
                  alt="twitter account link"
                  color="white"
                  fill="white"
                />
              </Link>
              <Link href="/" isExternal _hover={{ color: "primary.limeGreen" }}>
                <Image
                  src={PinterestIcon}
                  alt="pinterest account link"
                  color="white"
                  fill="white"
                />
              </Link>
              <Link href="/" isExternal _hover={{ color: "primary.limeGreen" }}>
                <Image
                  src={InstagramIcon}
                  alt="instagram account link"
                  color="white"
                  fill="white"
                />
              </Link>
            </HStack>
          </Box>
        </Flex>
        <Box
          fontSize={{ base: "xs" }}
          fontWeight="light"
          textAlign={{ base: "center", lg: "left" }}
          flex={{ lg: 1 }}
        >
          <Link as={RRLink} to="/" _hover={{ color: "primary.limeGreen" }}>
            <Box py="0.5rem">About Us</Box>
          </Link>
          <Link as={RRLink} to="/" _hover={{ color: "primary.limeGreen" }}>
            <Box py="0.5rem">Contact</Box>
          </Link>
          <Link as={RRLink} to="/" _hover={{ color: "primary.limeGreen" }}>
            <Box py="0.5rem">About Us</Box>
          </Link>
        </Box>

        <Box
          fontSize={{ base: "xs" }}
          fontWeight="light"
          textAlign={{ base: "center", lg: "left" }}
          flex={{ lg: 1 }}
        >
          <Link as={RRLink} to="/" _hover={{ color: "primary.limeGreen" }}>
            <Box py="0.5rem">Careers</Box>
          </Link>
          <Link as={RRLink} to="/" _hover={{ color: "primary.limeGreen" }}>
            <Box py="0.5rem">Support</Box>
          </Link>
          <Link as={RRLink} to="/" _hover={{ color: "primary.limeGreen" }}>
            <Box py="0.5rem">Privacy Policy</Box>
          </Link>
        </Box>
        <Box
          alignSelf={{ lg: "stretch" }}
          display={{ lg: "flex" }}
          flexDirection="column"
          alignItems="flex-end"
          justifyContent="space-between"
          textAlign={{ base: "center", lg: "right" }}
          flex={{ lg: 4 }}
        >
          <RoundedButton fontSize="xs">Request Invite</RoundedButton>

          <Text
            my={{ lg: "1rem" }}
            fontSize="xs"
            fontWeight="light"
            color="neutral.grayishBlue"
          >
            Easybank. All Rights Reserved
          </Text>
        </Box>
      </Flex>
      <div className="attribution">
        Challenge by{" "}
        <a
          href="https://www.frontendmentor.io?ref=challenge"
          rel="noreferrer"
          target="_blank"
        >
          Frontend Mentor
        </a>
        . Coded by{" "}
        <a href="/" rel="noreferrer" target="_blank">
          Oreoluwa Bimbo-Salami
        </a>
      </div>
    </Box>
  );
};

export default Footer;
