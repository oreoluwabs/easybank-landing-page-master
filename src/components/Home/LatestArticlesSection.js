import { Box, Heading, Image, Link, Text } from "@chakra-ui/react";
import { Link as RRLink } from "react-router-dom";

import CurrencyImage from "../../assets/images/image-currency.jpg";
import RestaurantImage from "../../assets/images/image-restaurant.jpg";
import PlaneImage from "../../assets/images/image-plane.jpg";
import ConfettiImage from "../../assets/images/image-confetti.jpg";

const Article = ({
  url = "/",
  imageURL = "",
  title = "Title",
  author = "Firstname Lastname",
  description = "Description",
}) => {
  return (
    <>
      <Box
        borderRadius="md"
        overflow="hidden"
        backgroundColor="white"
        my="1rem"
        mx={{ lg: "1rem" }}
        _first={{
          ml: { lg: 0 },
        }}
        _last={{
          mr: { lg: 0 },
        }}
        role="group"
      >
        <Link as={RRLink} to={url} _hover={{ textDecoration: "none" }}>
          <Box>
            <Image
              // boxSize={{ lg: "sm" }}
              // htmlHeight={{ lg: "500px" }}
              height={{ lg: "200px" }}
              width={{ lg: "100%" }}
              fit="cover"
              src={imageURL}
              fallbackSrc="https://via.placeholder.com/400X300"
              alt={title + "image"}
            />
          </Box>
          <Box padding="1.5rem">
            <Text fontSize="xs">By {author}</Text>
            <Heading
              my="0.5rem"
              fontWeight="normal"
              fontSize="md"
              color="primary.darkBlue"
              _groupHover={{ color: "primary.limeGreen" }}
            >
              {title}
            </Heading>
            <Text fontSize="md">{description}</Text>
          </Box>
        </Link>
      </Box>
    </>
  );
};

const LatestArticlesSection = () => {
  return (
    <Box as="section" backgroundColor="neutral.veryLightGray">
      <Box
        maxWidth={{ md: "85%" }}
        mx={{ md: "auto" }}
        color="neutral.grayishBlue"
        py={{ base: "2rem", lg: "4rem" }}
      >
        <Box
          px={{ base: "30px", md: 0 }}
          py={{ base: "0.5rem" }}
          textAlign={{ base: "center", lg: "left" }}
          flex={{ lg: "0 0 40%" }}
        >
          <Heading fontWeight="normal" fontSize="3xl" color="primary.darkBlue">
            Latest Articles
          </Heading>
        </Box>
        <Box
          px={{ base: "30px", md: 0 }}
          display="flex"
          flexDirection={{ base: "column", lg: "row" }}
          justifyContent={{ lg: "space-between" }}
        >
          <Article
            imageURL={CurrencyImage}
            title="Receive money in any currency with no fees"
            author="By Claire Robinson"
            description="The world is getting smaller and we're becoming more mobile. So why should you be forced to only receive money in a single..."
          />
          <Article
            imageURL={RestaurantImage}
            title="Treat yourself without worrying about money"
            author="By Wilson Hutton"
            description="Our simple budgeting feature allows you to separate out your spending and set realistic limits each month. That means you..."
          />
          <Article
            imageURL={PlaneImage}
            title="Take your Easybank card wherever you go"
            author="By Wilson Hutton"
            description="We want you to enjoy your travels. This is why we don't charge any fees on purchases while you're abroad. We'll even show you..."
          />
          <Article
            imageURL={ConfettiImage}
            title="Our invite-only Beta accounts are now live!"
            author="By Claire Robinson"
            description="After a lot of hard work by the whole team, we're excited to launch our closed beta. It's easy to request an invite through the site ..."
          />
        </Box>
      </Box>
    </Box>
  );
};

export default LatestArticlesSection;
