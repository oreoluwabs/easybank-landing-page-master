import { Box, Heading, Image, Text } from "@chakra-ui/react";
import OnlineIcon from "../../assets/images/icon-online.svg";
import OnboardingIcon from "../../assets/images/icon-onboarding.svg";
import BudgetingIcon from "../../assets/images/icon-budgeting.svg";
import ApiIcon from "../../assets/images/icon-api.svg";

const Benefit = ({
  icon = OnlineIcon,
  title = "Title",
  description = "Description",
}) => {
  return (
    <Box
      my={{ base: "30px" }}
      mx={{ lg: "1rem" }}
      _first={{
        ml: { lg: 0 },
      }}
      _last={{
        mr: { lg: 0 },
      }}
      display="flex"
      flexDirection={{ base: "column" }}
      justifyContent={{ base: "center" }}
      alignItems={{ base: "center", lg: "start" }}
    >
      <Image src={icon} alt="Online Banking" />
      <Heading
        my="1rem"
        fontWeight="normal"
        fontSize="xl"
        color="primary.darkBlue"
      >
        {title}
      </Heading>
      <Text fontSize={{ base: "sm", md: "md" }}>{description}</Text>
    </Box>
  );
};

const BenefitsSection = () => {
  return (
    <>
      <Box backgroundColor="gray.100" as="section" zIndex={-2}>
        <Box
          maxWidth={{ md: "85%" }}
          mx={{ md: "auto" }}
          color="neutral.grayishBlue"
          py={{ base: "2rem", lg: "4rem" }}
        >
          <Box
            px={{ base: "30px", md: 0 }}
            textAlign={{ base: "center", lg: "left" }}
            flex={{ lg: "0 0 40%" }}
          >
            <Heading
              fontWeight="normal"
              fontSize="3xl"
              color="primary.darkBlue"
            >
              Why choose Easybank?
            </Heading>
            <Text
              my="0.9rem"
              fontSize={{ base: "sm", md: "md" }}
              width={{ lg: "42%" }}
            >
              We leverage Open Banking to turn your bank account into your
              financial hub. Control your finances like never before.
            </Text>
          </Box>
          <Box
            px={{ base: "30px", md: 0 }}
            display="flex"
            flexDirection={{ base: "column", lg: "row" }}
            textAlign={{ base: "center", lg: "left" }}
            justifyContent={{ lg: "space-between" }}
          >
            <Benefit
              icon={OnlineIcon}
              title="Online Banking"
              description="Our modern web and mobile applications allow you to keep track of your
        finances wherever you are in the world."
            />
            <Benefit
              icon={BudgetingIcon}
              title="Simple Budgeting"
              description="See exactly where your money goes each month. Receive notifications when you're
            close to hitting your limits."
            />
            <Benefit
              icon={OnboardingIcon}
              title="Fast Onboarding"
              description="We don't do branches. Open your account in minutes online and start taking control
            of your finances right away."
            />
            <Benefit
              icon={ApiIcon}
              title="Open API"
              description="Manage your savings, investments, pension, and much more from one account. Tracking
          your money has never been easier."
            />
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default BenefitsSection;
