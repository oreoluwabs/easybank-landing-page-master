import {
  Box,
  Heading,
  Image,
  Text,
  useBreakpointValue,
} from "@chakra-ui/react";
import HeroImageMockUp from "../../assets/images/image-mockups.png";
import { RoundedButton } from "../../components/Button";

const BGImageMobile = require("../../assets/images/bg-intro-mobile.svg")
  .default;
const BGImageDesktop = require("../../assets/images/bg-intro-desktop.svg")
  .default;

const HeroSection = () => {
  const BGIMAGE = useBreakpointValue({
    base: BGImageMobile,
    lg: BGImageDesktop,
  });
  return (
    <>
      <Box
        backgroundImage={`url(${BGIMAGE})`}
        backgroundRepeat="no-repeat"
        backgroundSize={{
          base: "contain",
          xl: "70%",
          "2xl": "contain",
          "3xl": "auto",
        }}
        backgroundPosition={{
          lg: "top 55% right -65%",
          xl: "top 55% right -65%",
          "2xl": "top 55% right -21%",
          "3xl": "top 155% right -65%",
        }}
        minHeight={{ lg: "70vh" }}
      >
        <Box
          display={{ lg: "flex" }}
          alignItems={{ lg: "stretch" }}
          flexDirection={{ lg: "row-reverse" }}
          position={{ lg: "relative" }}
        >
          <Box
            flex={1}
            display="flex"
            justifyContent="center"
            position={{
              base: "relative",
              lg: "absolute",
            }}
            top={{ lg: "-8%" }}
            right={{ lg: "-17%", xl: "-8%" }}
          >
            <Image
              marginTop={{ base: "-16.5%", lg: 0 }}
              boxSize={{ base: "92%", lg: "80%", xl: "92%" }}
              src={HeroImageMockUp}
              fallbackSrc="https://via.placeholder.com/500"
              alt="Segun Adebayo"
            />
          </Box>
          <Box
            flex={1}
            color="neutral.grayishBlue"
            pb={{ base: "4rem" }}
            pt={{ base: "0.5rem" }}
            minHeight={{ lg: "70vh" }}
            maxW={{ lg: "85%" }}
            mx={{ lg: "auto" }}
            position={{ lg: "relative" }}
          >
            <Box
              position={{ lg: "absolute" }}
              top={{ lg: "50%" }}
              transform={{ lg: "translateY(-50%)" }}
              px={{ base: "30px", lg: 0 }}
              textAlign={{ base: "center", lg: "left" }}
              width={{ lg: "35%" }}
            >
              <Heading
                fontWeight="normal"
                fontSize="4xl"
                color="primary.darkBlue"
              >
                Next generation digital banking
              </Heading>
              <Text my="0.9rem">
                Take your financial life online. Your Easybank account will be a
                one-stop-shop for spending, saving, budgeting, investing, and
                much more.
              </Text>
              <Box as="span" mt="0.9rem">
                <RoundedButton>Request Invite</RoundedButton>
              </Box>
            </Box>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default HeroSection;
