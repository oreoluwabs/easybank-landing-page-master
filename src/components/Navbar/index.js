import {
  Box,
  Flex,
  IconButton,
  Image,
  Link,
  useBreakpointValue,
  useDisclosure,
} from "@chakra-ui/react";
import { Link as RRLink } from "react-router-dom";
// import { EasyBankLogo, HamburgerMenuIcon } from "../Icons";
import Logo from "../../assets/images/logo.svg";
import MenuIcon from "../../assets/images/icon-hamburger.svg";
import CloseIcon from "../../assets/images/icon-close.svg";
import { NavLinkItem } from "./NavLinkItem";
import { RoundedButton } from "../Button";

const NavLinks = ({ hideMenuIcon, navMenuState }) => {
  return (
    <Box
      zIndex="999"
      hidden={!hideMenuIcon && !navMenuState}
      position={{ base: "absolute", lg: "static" }}
      top={{ base: "60px", lg: 0 }}
      left={{ base: "0px", lg: 0 }}
      bgGradient={{
        base: "linear(to-b, primary.darkBlue, transparent)",
        lg: "none",
      }}
      width={{ base: "100%", lg: "auto" }}
      height={{ base: "calc(100vh - 60px)", lg: "auto" }}
      p={{ base: "30px", lg: 0 }}
    >
      {/* <Fade in={navMenuState && hideMenuIcon} unmountOnExit> */}
      <Flex
        flexDirection={{ base: "column", lg: "row" }}
        bg={{ base: "white", lg: "transparent" }}
        width="100%"
        borderRadius={{ base: "md", lg: 0 }}
        p={{ base: "1rem", lg: 0 }}
        // height={{ lg: "100%" }}
        // justify={{ base: "center", lg: "start" }}
        // alignItems={{ lg: "center" }}
        // justify={{ lg: "stretch" }}
      >
        <NavLinkItem to="/">Home</NavLinkItem>
        <NavLinkItem to="/">About</NavLinkItem>
        <NavLinkItem to="/">Contact</NavLinkItem>
        <NavLinkItem to="/">Blog</NavLinkItem>
        <NavLinkItem to="/" label="Careers" />
      </Flex>
      {/* </Fade> */}
    </Box>
  );
};

const Navbar = () => {
  const hideMenuIcon = useBreakpointValue({ base: false, lg: true });
  const { isOpen: navMenuState, onToggle: onToggleNavMenu } = useDisclosure(
    true
  );

  const showElementLargeScreen = !hideMenuIcon;
  return (
    <Box
      as="header"
      // position="sticky" width="100%"
      width={{ base: "100%" }}
      position={{ base: "absolute" }}
      zIndex="9"
      backgroundColor="white"
    >
      <Flex
        as="nav"
        height="60px"
        alignItems="center"
        justify="space-between"
        mx="auto"
        maxWidth="85%"
        zIndex="9"
      >
        <Link as={RRLink} to="/">
          <Image src={Logo} alt="Logo" />
        </Link>
        <IconButton
          onClick={onToggleNavMenu}
          background="transparent"
          _active={{ background: "transparent" }}
          _hover={{ background: "transparent" }}
          hidden={hideMenuIcon}
          aria-label="Toggle navigation menu"
          icon={
            navMenuState ? (
              <Image src={CloseIcon} alt="close menu" />
            ) : (
              <Image src={MenuIcon} alt="open menu" />
            )
          }
        />
        <NavLinks hideMenuIcon={hideMenuIcon} navMenuState={navMenuState} />
        <Box hidden={showElementLargeScreen}>
          {/* <Button>Request Invite</Button> */}
          <RoundedButton>Request Invite</RoundedButton>
        </Box>
      </Flex>
      {/* <Box height="100px" w="100%" bg="blue.500"></Box> */}
    </Box>
  );
};

export default Navbar;
