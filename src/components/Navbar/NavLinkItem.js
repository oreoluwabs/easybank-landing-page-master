import { Box, Link } from "@chakra-ui/react";
import { NavLink as RRNLink } from "react-router-dom";

export const NavLinkItem = ({ label, to, children }) => {
  return (
    <Box
      role="group"
      transition="all 0.3s ease-in"
      py={{ base: "0.7rem", lg: 0 }}
      px={{ lg: "0" }}
      _notLast={{ mr: { lg: "2rem" } }}
      _hover={{
        transition: "all 0.3s ease-out",
        bgGradient: {
          base: "linear(to-r, primary.limeGreen, primary.brightCyan)",
          lg: "none",
        },
        borderRadius: { base: "md", lg: 0 },
        color: { base: "neutral.white", lg: "primary.darkBlue" },
      }}
      height={{ lg: "100%" }}
      textAlign={{ base: "center" }}
      color={{ lg: "neutral.grayishBlue" }}
      fontSize={{ base: "1.2rem", lg: "0.8rem" }}
      position={{ lg: "relative" }}
    >
      <Link
        as={RRNLink}
        to={to}
        _hover={{ textDecoration: "none" }}
        _visited={{ textDecoration: "none" }}
        _active={{ textDecoration: "none" }}
        _focus={{ boxShadow: "none" }}
      >
        <Box>{children || label}</Box>
      </Link>
      <Box
        height={{ base: 0, lg: "5px" }}
        position={{ lg: "absolute" }}
        bottom={{ lg: "-85%" }}
        width="100%"
        opacity={0}
        transition="all 0.5s ease-in"
        transform="scaleX(.2)"
        _groupHover={{
          transition: "all 0.5s ease-out",
          transform: "scaleX(1)",
          opacity: 1,
          bgGradient: {
            lg: "linear(to-r, primary.limeGreen, primary.brightCyan)",
          },
        }}
      />
    </Box>
  );
};
