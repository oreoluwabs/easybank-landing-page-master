import { BrowserRouter, Route, Switch } from "react-router-dom";
import Footer from "./components/Footer";

import Navbar from "./components/Navbar";
import RootProviders from "./store/context";
import { HomePage } from "./views";

function App() {
  return (
    <>
      <BrowserRouter>
        <RootProviders>
          <Navbar />
          <Switch>
            <Route exact path="/" component={HomePage} />
          </Switch>
          <Footer />
        </RootProviders>
      </BrowserRouter>
    </>
  );
}

export default App;
